This is a tool for operating my Yamaha HTR-6030 receiver.

Purpose is to control/query the inputs and the volume remotely and
integrate it into players like mpd and shairport. The final goal
is to use it as "mixer", i.e. volume changes ftom the players
is applied only on the receiver, and volume changes on the receiver
are reported back to the players.

It requires a piece of hardware hooked to the receiver to intercept
the volume settings. I'm using a teensy 4.0, connected via USB to
this program, and to the receiver via GPIOs to an internal serial bus.

See https://bitbucket.org/fesc2000/yamaha_sniffer.git

To control the receiver i'm using lirc and an irtoy dongle.
Consequently, liblirc_client is required.
The lirc configuration file (RAV28.lircd.conf) is part of the repo.

On the os side, for now the tool polls a "volume file" where the target 
volume is set, and a "volume_current" file where we report the current
transceiver volume.

It's WIP, but the basic control loop works.
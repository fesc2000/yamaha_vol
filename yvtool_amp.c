/* yvtool_amp.c
 *
 * Copyright 2022 Felix Schmidt
 * License: GPLv3 or later.
 *
 * Handle console responses/events from amplifier
 */

#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <ctype.h>
#include "yvtool.h"

int   ttyfd         = -1;

void reset_amp_data(struct amp_data *data)
{
  data->db        = VOL_INVALID;
  data->frontl    = VOL_INVALID;
  data->frontr    = VOL_INVALID;
  data->center    = VOL_INVALID;
  data->surroundl = VOL_INVALID;
  data->surroundr = VOL_INVALID;
  strcpy(data->input, INPUT_UNKNOWN);
}

bool find_token(char **s, char *token)
{
  bool rc = false;
  char *pos;

  pos = strstr(*s, token);
  if (pos)
  {
    pos += strlen(token);
    *s = pos;
    rc = true;
  }
  return rc;
}

bool next_token(char **s)
{
  return find_token(s, ":");
}

bool int_token(char **buffer, char *name, int *val)
{
  int oval = *val;
  char *input = *buffer;

  if (!strncmp(input, name, strlen(name)))
  {
    input += strlen(name);

    if ((1 == sscanf(input, "%d", val)))
    {
      *buffer = input;
      return true;
    }
  }
  *val = oval;
  return false;
}

bool string_token(char **buffer, char *name, char *val)
{
  int i;

  char *input = *buffer;

  if (!strncmp(input, name, strlen(name)))
  {
    input += strlen(name);

    for (i = 0; i < MAX_INPUT_SIZE-1; i++)
    {
      if (!isprint(input[i]) || isspace(input[i]) || (input[i] == '\n'))
      {
        break;
      }
      else
      {
        val[i] = input[i];
      }
    }
    val[i] = '\0';
    *buffer = &input[i];

    return true;
  }
  return false;
}

bool parse_buffer(char *buffer, struct amp_data* amp_data)
{
  bool rc = false;

  while (next_token(&buffer))
  {
    if (int_token(&buffer, TOKEN_DB, &amp_data->db))
    {
      rc = true;
      continue;
    }
    if (int_token(&buffer, TOKEN_FRONT_L, &amp_data->frontl))
    {
      rc = true;
      continue;
    }
    if (int_token(&buffer, TOKEN_FRONT_R, &amp_data->frontr))
    {
      rc = true;
      continue;
    }
    if (int_token(&buffer, TOKEN_CENTER, &amp_data->center))
    {
      rc = true;
      continue;
    }
    if (int_token(&buffer, TOKEN_SURROUND_L, &amp_data->surroundl))
    {
      rc = true;
      continue;
    }
    if (int_token(&buffer, TOKEN_SURROUND_R, &amp_data->surroundr))
    {
      rc = true;
      continue;
    }
    if (string_token(&buffer, TOKEN_INPUT, amp_data->input))
    {
      rc = true;
      continue;
    }
  }
  return rc;
}

static void mqtt_publish(struct amp_data *amp_data)
{
  mqtt_publish_string("input", amp_data->input, true);
  mqtt_publish_int("db", amp_data->db, true);
  mqtt_publish_int("frontl", amp_data->frontl, true);
  mqtt_publish_int("frontr", amp_data->frontr, true);
  mqtt_publish_int("center", amp_data->center, true);
  mqtt_publish_int("surroundl", amp_data->surroundl, true);
  mqtt_publish_int("surroundr", amp_data->surroundr, true);
}

void *amp_thread(void *arg)
{
  char            buffer[1000];
  size_t          sz;
  struct sm_event event;
  struct amp_data old_amp_data;
  struct amp_data amp_data;

  reset_amp_data(&old_amp_data);
  reset_amp_data(&amp_data);

  while (true)
  {
    if (!check_file(&ttyfd, sniffer_name, true))
    {
      sleep(1);
      continue;
    }

    sz = read(ttyfd, buffer, sizeof(buffer));

    if (-1 == sz)
    {
      perror(sniffer_name);
      close(ttyfd);
      ttyfd = -1;
      continue;
    }

    if (0 == sz)
    {
      continue;
    }

    buffer[sz] = 0;

    if (parse_buffer(buffer, &amp_data))
    {
      event.event = e_valid_response;
      strcpy (event.input, "");
      sm_event_send(&event);

      mqtt_publish(&amp_data);

      if (strcmp(old_amp_data.input, amp_data.input))
      {
        memset ((void*)&event, 0, sizeof(event));
        event.event = e_input_sel;
        event.volume = 0;
        strcpy (event.input, amp_data.input);

        sm_event_send(&event);
      }

      if ((amp_data.db != VOL_INVALID) && (amp_data.db != old_amp_data.db))
      {
        event.event = e_vol_change;
        event.volume = db_to_volume(amp_data.db);
        sm_event_send(&event);
      }
    }

    old_amp_data = amp_data;
  }
}

/* trigger a read to get current amp settings */
void amp_trigger_read()
{
  for (int try = 0; try < 2; try++)
  {
    if (check_file(&ttyfd, sniffer_name, true))
    {
      if (-1 == write(ttyfd, "\n", sizeof("\n")))
      {
	perror(sniffer_name);
	(void)close(ttyfd);
	ttyfd = -1;
      }
      else
      {
	break;
      }
    }
  }
}

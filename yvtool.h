/* yvtool.h
 *
 * Copyright 2022 Felix Schmidt
 * License: GPLv3 or later.
 */
#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#define MAX_INPUT_SIZE  32
#define VOL_INVALID     (-9999)
#define INPUT_UNKNOWN   "unknown"

#define TOKEN_DB          "dB="
#define TOKEN_INPUT       "input="
#define TOKEN_FRONT_L     "FrontL="
#define TOKEN_FRONT_R     "FrontR="
#define TOKEN_CENTER      "Center="
#define TOKEN_SURROUND_L  "SurroundL="
#define TOKEN_SURROUND_R  "SurroundR="

enum sm_event_type
{
  e_vol_change = 0,
  e_input_sel,
  e_target_set,
  e_watchdog,
  e_valid_response,
  e_enter_state,
  e_exit_state,
  e_max_event
};
#if defined(DECL_SM)
const char* event_names[] =
{
  [e_vol_change]      = "e_vol_change",
  [e_input_sel]       = "e_input_sel",
  [e_target_set]      = "e_target_set",
  [e_watchdog]        = "e_watchdog",
  [e_valid_response]  = "e_response",
  [e_enter_state]     = "e_enter_state",
  [e_exit_state]      = "e_exit_state",
  [e_max_event]       = "e_max_event"
};
#endif

enum sm_state
{
  st_init1 = 0,   /* initializing, checking for USB responding */
  st_init2,       /* initializing, searching for current input */
  st_idle,        /* idling, when audio input is not selected */
  st_playing,     /* playing audio */
  st_increasing,  /* increasing volume to target volume */
  st_decreasing,  /* decreasing volume to target volume */
  st_max_state
};
#if defined(DECL_SM)
const char* state_names[] =
{
  [st_init1]      = "st_init1",
  [st_init2]      = "st_init2",
  [st_idle]       = "st_idle",
  [st_playing]    = "st_playing",
  [st_increasing] = "st_increasing",
  [st_decreasing] = "st_decreasing",
  [st_max_state]  = "st_max_state"
};
#endif

struct amp_data
{
  int   db;
  char  input[MAX_INPUT_SIZE];
  int   frontl;
  int   frontr;
  int   center;
  int   surroundl;
  int   surroundr;
};

struct sm_event
{
  enum sm_event_type event;

  int   volume;
  char  input[MAX_INPUT_SIZE];
};

struct sm_data
{
  enum sm_state   state;                  /* current state */
  int             volume;                 /* current volume */
  int             target;                 /* target volume */
  bool            input_seek_toggle;      /* toggler for input seek */
  int             input_seek_count;       /* number of input seek attempts */
  struct timespec next_wd;                /* next WD timeout */
  char            input[MAX_INPUT_SIZE];  /* current input */

  int             chase_retry;            /* blind attempts to set volume */
};

extern char*       sniffer_name;
extern const char* active_amp;

extern char *lirc_control;
extern char *vol_up_cmd;
extern char *vol_down_cmd;
extern char *select_play_input;
extern char *select_noplay_input;

void sm_init();
void* sm_thread(void* arg);
void sm_event_send(struct sm_event *event);
void *amp_thread(void *arg);
void amp_trigger_read();

int db_to_volume(int db);
int volume_to_db(int volume);
bool check_file(int *fd, char *fname, bool is_serial);
void report_current_volume(int volume);
void set_target_volume(int volume);
void watchdog_trigger();

void yvtool_lirc_init();
bool yvtool_lirc_volup(int vol_diff);
bool yvtool_lirc_voldown(int vol_diff);
bool yvtool_lirc_inputsel(bool play);

void mqtt_enable(bool enable);
void mqtt_publish_string(const char *name, char *payload, bool retain);
void mqtt_publish_int(const char *name, int val, bool retain);

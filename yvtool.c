/* Amplifier volume control tool
 *
 * Copyright 2022 Felix Schmidt
 *
 * License: GPLV3
 */
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <ctype.h>
#include <signal.h>
#include "lirc_client.h"

#include "yvtool.h"

const char *usage = " %s [-h] [-v] [-m] [-d tty-dev] [-i mindb] [-a maxdb] [-t target-vol-file] [-c current-vol-file]\n";

char* sniffer_name  = "/dev/ttyACM0";
char* target_file   = "/tmp/volume";
char* current_file  = "/tmp/volume_cur";
int   mindb;
int   maxdb;
int   wdog_count    = 0;

const char* active_amp = "CD";

struct termios serial_parms =
{
  .c_iflag = IGNBRK,
  .c_cflag = CS8|CREAD,
};

void watchdog_handler(int sig)
{
  if (0 == wdog_count++)
  {
    fprintf(stderr, "Watchdog pre-trigger!\n");
    watchdog_trigger();
  }
  else
  {
    fprintf(stderr, "Watchdog timeout, exiting!\n");
    exit(1);
  }
}

void watchdog_trigger()
{
  alarm(10);
  signal(SIGALRM, watchdog_handler);
}

void report_current_volume(int volume)
{
  FILE *fd = fopen(current_file, "w+");

  if (NULL == fd)
  {
    perror(current_file);
  }
  else
  {
    fprintf(fd, "%d", volume);
    fclose(fd);
  }

  mqtt_publish_int("volume", volume, true);
}

void set_target_volume(int volume)
{
  FILE *fd = fopen(target_file, "w+");

  mqtt_publish_int("target", volume, true);

  if (NULL == fd)
  {
    perror(target_file);
  }
  else
  {
    fprintf(fd, "%d", volume);
    fclose(fd);
  }
}

int db_to_volume(int db)
{
  int rc;
  double db_normalized;
  double db_range = maxdb - mindb;

  if (db < mindb)
  {
    db_normalized = mindb;
  }
  else if (db > maxdb)
  {
    db_normalized = maxdb;
  }
  else
  {
    db_normalized = db;
  }

  db_normalized -= mindb;

  rc = (int)((db_normalized / db_range) * 100.0);

  return rc;
}

int volume_to_db(int volume)
{
  return mindb + (int)(((double)(maxdb - mindb) * (double)volume) / 100.0);
}

bool check_file(int *fd, char *fname, bool is_serial)
{
  if (-1 == *fd)
  {
    *fd = open(fname, O_RDWR);

    if ((*fd >= 0) && is_serial)
    {
      if (-1 == tcgetattr(*fd, &serial_parms))
      {
        perror("tcgetattr");
        return false;
      }

      serial_parms.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                    INLCR | PARMRK | INPCK | ISTRIP | IXON);
      serial_parms.c_oflag = 0;
      serial_parms.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
      serial_parms.c_cflag &= ~(CSIZE | PARENB);
      serial_parms.c_cflag |= CS8;
      cfsetspeed(&serial_parms, B115200);

      if (-1 == tcsetattr(*fd, TCSAFLUSH, &serial_parms))
      {
        perror("tcsetattr");
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  return true;
}

void* watcher_thread(void* arg)
{
  char            buffer[30];
  int             target;
  struct sm_event event;
  int             notify_fd;
  int             watch;
  struct inotify_event ino_event;
  int             fd = -1;

  notify_fd = inotify_init();
  if (-1 == notify_fd)
  {
    perror("inotify_init");
    exit(1);
  }

  watch = inotify_add_watch( notify_fd, target_file, IN_CREATE | IN_DELETE | IN_MODIFY);
  if (-1 == watch)
  {
    perror("inotify_add_watch");
    exit(1);
  }

  check_file(&fd, target_file, false);

  while (true)
  {
    if (fd == -1)
    {
      check_file(&fd, target_file, false);
    }

    if (fd != -1)
    {
      memset(buffer, 0, sizeof(buffer));
      if (read(fd, buffer, sizeof(buffer)) > 0)
      {
        target = -1;
        if (1 == sscanf(buffer, "%d", &target))
        {
          if ((target >= 0) && (target <= 100))
          {
            event.event = e_target_set;
            event.volume = target;
            sm_event_send(&event);
          }
        }
      }
      lseek(fd, 0, SEEK_SET);
    }
    else
    {
      check_file(&fd, target_file, false);
    }

    if (read(notify_fd, &ino_event, sizeof(ino_event)) == -1)
    {
      perror("inotify read");
      exit(1);
    }
    else
    {
      if (ino_event.mask & (IN_CREATE | IN_DELETE))
      {
        close(fd);
        fd = -1;
      }
    }
  }
}

int main(int argc, char **argv)
{
  int   c;
  bool  mindb_set = false;
  bool  maxdb_set = false;

  static struct option long_options[] = {
    { "help",    no_argument,	      NULL, 'h' },
    { "version", no_argument,       NULL, 'v' },
    { "device",  required_argument, NULL, 'd' },
    { "mindb",   required_argument, NULL, 'i' },
    { "maxdb",   required_argument, NULL, 'a' },
    { "target",  required_argument, NULL, 't' },
    { "current", required_argument, NULL, 'c' },
    { "mqtt",    no_argument,       NULL, 'm' },
    { 0,	     0,			0,    0	  }
  };
  while ((c = getopt_long(argc, argv, "hvd:i:a:m", long_options, NULL)) != -1)
  {
    switch (c) {
      case 'd':
        sniffer_name = optarg;
        break;

      case 'i':
        mindb = atoi(optarg);
        mindb_set = true;
        break;

      case 'a':
        maxdb = atoi(optarg);
        maxdb_set = true;
        break;

      case 't':
        target_file = optarg;
        break;

      case 'c':
        current_file = optarg;
        break;

      case 'm':
	mqtt_enable(true);
	break;

      default:
        fprintf (stderr, usage, argv[0]);
        exit(1);
    }
  }

  if (!mindb_set && !maxdb_set)
  {
    mindb = -88;
    maxdb = -40;     // 17
  }
  else if (!mindb_set || !maxdb_set)
  {
    fprintf (stderr, "%s: both mindb and maxdb must be given\n", argv[0]);
    exit(1);
  }
  else if (maxdb <= mindb)
  {
    fprintf (stderr, "%s: bad volume range\n", argv[0]);
    exit(1);
  }

  sm_init();
  yvtool_lirc_init();

  pthread_t thread;
  if (pthread_create(&thread, NULL, sm_thread, NULL))
  {
    perror("sm_thread");
    exit(1);
  }

  if (pthread_create(&thread, NULL, amp_thread, NULL))
  {
    perror("amp_thread");
    exit(1);
  }

  if (pthread_create(&thread, NULL, watcher_thread, NULL))
  {
    perror("watcher_thread");
    exit(1);
  }

  while (true)
  {
    sleep(100);
  }

  return 0;
}

/* yvtool_mqtt.c
 *
 * Copyright 2023 Felix Schmidt
 * License: GPLv3 or later.
 *
 * MQTT client
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <mosquitto.h>
#include "yvtool.h"

static struct mosquitto *mq_client = NULL;
static bool connected = false;
static bool mqtt_enabled = false;

void mqtt_enable(bool enable)
{
  mqtt_enabled = enable;
}

static void mq_cleanup()
{
  if (connected)
  {
    (void)mosquitto_disconnect(mq_client);
    connected = false;
  }
  if (mq_client)
  {
    mosquitto_destroy(mq_client);
    mq_client = NULL;
  }
  mosquitto_lib_cleanup();
}

void mq_disc_cb(struct mosquitto *mq_client, void *arg, int rc)
{
  connected = false;

  if (rc != 0)
  {
    printf ("unexpected disconnect\n");
  }
}
	

static bool mq_init()
{
  static bool initialized = false;

  if (!mqtt_enabled)
    return false;

  if (!initialized)
  {
    mosquitto_lib_init();

    mq_client = mosquitto_new(NULL, true, NULL);

    if (!mq_client)
    {
      perror("mosquitto_new");
      exit(1);
    }

    mosquitto_threaded_set(mq_client, false);
    mosquitto_disconnect_callback_set(mq_client, mq_disc_cb);

    initialized = true;
    atexit(mq_cleanup);
  }

  if (!connected)
  {
    int rc = mosquitto_connect(mq_client, "127.0.0.1", 1883, 10);
    if (MOSQ_ERR_SUCCESS == rc)
    {
      printf ("mqtt: connected\n");
      connected = true;
    }
    else
    {
      fprintf(stderr, "mqtt connect failed: %s\n", mosquitto_strerror(rc));
    }
  }

  return connected;
}

void mqtt_publish_string(const char *name, char *payload, bool retain)
{
  char topic[128];
  sprintf(topic, "yamaha_amp/%s", name);

  if (!mq_init())
    return;

  int rc = mosquitto_publish(mq_client, NULL, topic, strlen(payload), payload, 0, retain);

  if (rc != MOSQ_ERR_SUCCESS)
  {
    fprintf (stderr, "mosquitto_publish [%s] = [%s]: rc=%d/%s\n", topic, payload, rc, mosquitto_strerror(rc));

    (void)mosquitto_disconnect(mq_client);
    connected = false;
  }
  else
  {
    printf ("mosquitto_publish [%s] = [%s]\n", topic, payload);
  }
}

void mqtt_publish_int(const char *name, int val, bool retain)
{
  char payload[128];

  sprintf(payload, "%d", val);

  mqtt_publish_string(name, payload, retain);
}


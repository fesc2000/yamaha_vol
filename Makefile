OBJECTS=yvtool.o yvtool_amp.o yvtool_sm.o yvtool_lirc.o yvtool_mqtt.o

all:	yvtool

yvtool: $(OBJECTS)
	$(CC) -o $@ $(OBJECTS) -pthread -llirc_client -lmosquitto

.c.o: 
	$(CC) -c -o $@ $< -Wall -Werror

$(OBJECTS):	yvtool.h

clean:
	rm -f *.o yvtool

install:
	@cp -a yvtool $(INSTDIR)/bin

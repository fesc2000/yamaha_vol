/* yvtool_lirc.c
 *
 * Copyright 2022 Felix Schmidt
 * License: GPLv3 or later.
 *
 * lirc interface to control amplifier
 */

#include <getopt.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>

#include "yvtool.h"
#include "lirc_client.h"

static int fd;
char *lirc_control = "RAV28";

char *vol_up_cmd          = "AMP_VOL_UP";
char *vol_down_cmd        = "AMP_VOL_DOWN";
char *select_play_input   = "AMP_CD";
char *select_noplay_input = "AMP_DVD";

static int send_packet(lirc_cmd_ctx* ctx, int fd)
{
  int r;

  do {
    r = lirc_command_run(ctx, fd);
    if (r != 0 && r != EAGAIN)
      fprintf(stderr,
        "Error running command: %s\n", strerror(r));
  } while (r == EAGAIN);
  return r == 0 ? 0 : -1;
}

void yvtool_lirc_init()
{
  fd = lirc_get_local_socket("/var/run/lirc/lircd", 0);

  if (fd == -1)
  {
    perror("/var/run/lirc/lircd");
    exit(1);
  }
}

bool yvtool_lirc_volup(int vol_diff)
{
  lirc_cmd_ctx ctx;

  if (lirc_command_init(&ctx, "SEND_ONCE %s %s\n", lirc_control, vol_up_cmd) != 0)
  {
    fprintf (stderr, "lirc_command_init\n");
    exit(1);
  }
  
  if (send_packet(&ctx, fd) == -1)
  {
    fprintf (stderr, "send_packet\n");
    return false;
  }

  return true;
}

bool yvtool_lirc_voldown(int vol_diff)
{
  lirc_cmd_ctx ctx;

  if (lirc_command_init(&ctx, "SEND_ONCE %s %s\n", lirc_control, vol_down_cmd) != 0)
  {
    fprintf (stderr, "lirc_command_init\n");
    exit(1);
  }
  
  if (send_packet(&ctx, fd) == -1)
  {
    fprintf (stderr, "send_packet\n");
    return false;
  }

  return true;
}

bool yvtool_lirc_inputsel(bool play)
{
  lirc_cmd_ctx ctx;

  printf ("[selecting %s]", play ? select_play_input : select_noplay_input);
  fflush(stdout);

  if (lirc_command_init(&ctx, "SEND_ONCE %s %s\n", lirc_control, play ? select_play_input : select_noplay_input) != 0)
  {
    fprintf (stderr, "lirc_command_init\n");
    exit(1);
  }
  
  if (send_packet(&ctx, fd) == -1)
  {
    fprintf (stderr, "send_packet\n");
    return false;
  }

  return true;
}
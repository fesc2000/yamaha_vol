/* yvtool_sm.c
 *
 * Copyright 2022 Felix Schmidt
 * License: GPLv3 or later.
 *
 * State machine
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <getopt.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/inotify.h>
#include <ctype.h>

#define DECL_SM
#include "yvtool.h"

#define EVENT_Q_SIZE 100

typedef enum sm_state (*event_handler)(struct sm_data* sm, struct sm_event* event);

pthread_mutex_t   sm_mutex;
pthread_cond_t    sm_cv;
struct sm_event   eventq[EVENT_Q_SIZE];
int               eventq_head;
int               eventq_tail;

/*** Default handlers */

enum sm_state eh_dfl_vol_change(struct sm_data* sm, struct sm_event* event)
{
  sm->volume = event->volume;

  if (!strcmp(sm->input, active_amp))
  {
    report_current_volume(sm->volume);
  }

  return sm->state;
}

enum sm_state eh_dfl_input_sel(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  strcpy(sm->input, event->input);

  if (strcmp(sm->input, active_amp))
  {
    new_state = st_idle;
  }
  return new_state;
}

enum sm_state eh_dfl_target_set(struct sm_data* sm, struct sm_event* event)
{
  sm->target = event->volume;
  return sm->state;
}

enum sm_state eh_dfl_watchdog(struct sm_data* sm, struct sm_event* event)
{
  return sm->state;
}

enum sm_state eh_dfl_event(struct sm_data* sm, struct sm_event* event)
{
  return sm->state;
}

/*** st_init1 handlers */

/* Request reading until we get something */
enum sm_state eh_init1_watchdog(struct sm_data* sm, struct sm_event* event)
{
  amp_trigger_read();

  return sm->state;
}

enum sm_state eh_init1_response(struct sm_data* sm, struct sm_event* event)
{
  return st_init2;
}

enum sm_state eh_init1_input_sel(struct sm_data* sm, struct sm_event* event)
{
  strcpy(sm->input, event->input);
  return st_init1;
}

enum sm_state eh_init1_vol_change(struct sm_data* sm, struct sm_event* event)
{
  sm->volume = event->volume;
  return st_init1;
}


/*** st_init2 handlers */

enum sm_state eh_init2_enter(struct sm_data* sm, struct sm_event* event)
{
  sm->input_seek_count = 0;
  return sm->state;
}

/* Toggle input until we get something other than "unknown" */
enum sm_state eh_init2_watchdog(struct sm_data* sm, struct sm_event* event)
{
  /* check if volume and input are known
   */
  if (sm->volume != VOL_INVALID)
  {
    if (!strcmp(sm->input, active_amp))
    {
      /* got audio input, resume to playing */ 
      return st_playing;
    }
    else if (strcmp(sm->input, INPUT_UNKNOWN))
    {
      /* got some input other than unknown.
      */
      if (sm->input_seek_count > 0)
      {
        /* if we were seeking for any input, request playing mode
        */
        yvtool_lirc_inputsel(true);
        return st_init2;
      }
      return st_idle;
    }
  }

  /* toggle input several times. Give up afterwards and
   * retry once user changes target volume */
  if (sm->input_seek_count < 5)
  {
    sm->input_seek_count++;

    yvtool_lirc_inputsel(sm->input_seek_toggle);
    sm->next_wd.tv_sec = 2;

    sm->input_seek_toggle = !sm->input_seek_toggle;
  }

  return sm->state;
}

enum sm_state eh_init2_input_sel(struct sm_data* sm, struct sm_event* event)
{
  strcpy(sm->input, event->input);
  return sm->state;
}

enum sm_state eh_init2_target_set(struct sm_data* sm, struct sm_event* event)
{
  /* if user requests volume change, and we timed out seeking for a valid input
   * retry the process
   */
  sm->input_seek_count = 0;
  return sm->state;
}

/*** st_idle handlers */
enum sm_state eh_idle_input_sel(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  strcpy(sm->input, event->input);

  if (!strcmp(sm->input, active_amp))
  {
    /* got input for audio out, resume to playing */ 
    new_state = st_playing;
  }

  return new_state;
}

/*** st_playing handlers */
enum sm_state eh_playing_enter(struct sm_data* sm, struct sm_event* event)
{
  report_current_volume(sm->volume);
  set_target_volume(sm->volume);

  return sm->state;
}

enum sm_state eh_playing_vol_change(struct sm_data* sm, struct sm_event* event)
{
  sm->volume = event->volume;
  report_current_volume(sm->volume);
  set_target_volume(sm->volume);
  return sm->state;
}

/* Target volume request */
enum sm_state eh_playing_target_set(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  if (event->volume > sm->volume)
  {
    yvtool_lirc_volup(1);
    new_state = st_increasing;
  }
  else if (event->volume < sm->volume)
  {
    yvtool_lirc_voldown(1);
    new_state =  st_decreasing;
  }

  sm->target = event->volume;

  return new_state;
}

/*** st_increasing handlers */

/* change of target value while chasing it */
enum sm_state eh_increasing_target_set(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  sm->target = event->volume;

  if (sm->target < sm->volume)
  {
    /* change seek direction */
    new_state = st_decreasing;
  }
  return new_state;
}

/* volume change while chasing target. Send next volume change command or end */
enum sm_state eh_increasing_vol_change(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  sm->chase_retry = 0;
  sm->volume = event->volume;
  report_current_volume(sm->volume);

  if (event->volume >= sm->target)
  {
    /* target reached */
    new_state = st_playing;
  }
  else
  {
    yvtool_lirc_volup(sm->target - event->volume);
  }

  return new_state;
}

/* timeout while chasing target. Retry or give up */
enum sm_state eh_increasing_watchdog(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  sm->chase_retry++;

  if (sm->chase_retry < 4)
  {
    yvtool_lirc_volup(sm->target - event->volume);
  }
  else
  {
    /* give up */
    set_target_volume(sm->volume);
    new_state = st_init1;
  }
  return new_state;
}

/*** st_decreasing handlers */

/* change of target value while chasing it */
enum sm_state eh_decreasing_target_set(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  sm->target = event->volume;
  if (sm->target > sm->volume)
  {
    /* change seek direction */
    new_state = st_increasing;
  }
  return new_state;
}

/* volume change while chasing target. Send next volume change command or end */
enum sm_state eh_decreasing_vol_change(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  sm->chase_retry = 0;
  sm->volume = event->volume;

  report_current_volume(sm->volume);
  if (event->volume <= sm->target)
  {
    /* target reached */
    new_state = st_playing;
  }
  else
  {
    yvtool_lirc_voldown(sm->target - event->volume);
  }
  return new_state;
}

/* timeout while chasing target. Retry or give up */
enum sm_state eh_decreasing_watchdog(struct sm_data* sm, struct sm_event* event)
{
  enum sm_state new_state = sm->state;

  sm->chase_retry++;

  if (sm->chase_retry < 4)
  {
    yvtool_lirc_voldown(0);
  }
  else
  {
    /* give up */
    set_target_volume(sm->volume);
    new_state = st_init1;
  }
  return new_state;
}

/* state/event handling jump table. */
event_handler sm_array[st_max_state+1][e_max_event+1] =
{
  [st_init1] = {
    [e_watchdog]        = eh_init1_watchdog,
    [e_valid_response]  = eh_init1_response,
    [e_input_sel]       = eh_init1_input_sel,
    [e_vol_change]      = eh_init1_vol_change
  },
  [st_init2] = {
    [e_enter_state] = eh_init2_enter,
    [e_watchdog]    = eh_init2_watchdog,
    [e_input_sel]   = eh_init2_input_sel,
    [e_target_set]  = eh_init2_target_set
  },
  [st_idle] = {
    [e_input_sel]   = eh_idle_input_sel
  },
  [st_playing] = {
    [e_enter_state] = eh_playing_enter,
    [e_vol_change]  = eh_playing_vol_change,
    [e_target_set]  = eh_playing_target_set
  },
  [st_increasing] = {
    [e_vol_change]  = eh_increasing_vol_change,
    [e_target_set]  = eh_increasing_target_set,
    [e_watchdog]    = eh_increasing_watchdog
  },
  [st_decreasing] = {
    [e_vol_change]  = eh_decreasing_vol_change,
    [e_target_set]  = eh_decreasing_target_set,
    [e_watchdog]    = eh_decreasing_watchdog
  },
  [st_max_state] = {
    [e_vol_change]  = eh_dfl_vol_change,
    [e_input_sel]   = eh_dfl_input_sel,
    [e_target_set]  = eh_dfl_target_set,
    [e_watchdog]    = eh_dfl_watchdog,
    [e_max_event]   = eh_dfl_event
  },
};

/** Enqueue event
 */
void sm_event_send(struct sm_event *event)
{
  int next_head;
  bool queue_full;

  do
  {
    queue_full = false;

    pthread_mutex_lock (&sm_mutex);

    next_head = eventq_head + 1;
    if (next_head >= EVENT_Q_SIZE)
      next_head = 0;

    if (next_head == eventq_tail)
    {
      fprintf (stderr, "Event queue full\n");
      queue_full = true;
    }
    else
    {
      eventq[eventq_head] = *event;
      eventq_head       = next_head;
      pthread_cond_signal (&sm_cv);
    }

    pthread_mutex_unlock (&sm_mutex);

    if (queue_full)
    {
      sleep(1);
    }

  } while (queue_full);
}

/** Dequeue event, or provide watchdog event when timing out.
 */
struct sm_event* sm_event_get(struct sm_data *sm)
{
  struct sm_event*        rc = NULL;
  struct timespec         ts;
  static struct sm_event  watchdog =
  {
    .event = e_watchdog
  };

  pthread_mutex_lock (&sm_mutex);

  if (eventq_head == eventq_tail)
  {
    clock_gettime(CLOCK_REALTIME, &ts);
    ts.tv_nsec += sm->next_wd.tv_nsec;
    ts.tv_sec  += sm->next_wd.tv_sec + ts.tv_nsec / 1000000000;
    ts.tv_nsec %= 100000000;

    switch (pthread_cond_timedwait(&sm_cv, &sm_mutex, &ts))
    {
      case 0:
        break;
      case ETIMEDOUT:
        rc = &watchdog;
        break;
      default:
        perror("pthread_cond_timedwait");
        exit(1);
    }
    sm->next_wd.tv_sec  = 1;
    sm->next_wd.tv_nsec = 0;
  }
  
  if (eventq_head != eventq_tail)
  {
    rc = &eventq[eventq_tail];

    eventq_tail++;
    if (eventq_tail >= EVENT_Q_SIZE)
      eventq_tail = 0;
  }

  pthread_mutex_unlock (&sm_mutex);

  return rc;
}

void sm_init()
{
  pthread_cond_init (&sm_cv, NULL);
  pthread_mutex_init (&sm_mutex, NULL);
  eventq_head = eventq_tail = 0;
}

void* sm_thread(void* arg)
{
  struct sm_data    sm;
  event_handler     handler;
  struct sm_event*  event;

  sm.state = st_init1;
  sm.volume = VOL_INVALID;
  sm.next_wd.tv_sec  = 2;
  sm.next_wd.tv_nsec = 0;
  strcpy(sm.input, INPUT_UNKNOWN);

  /* trigger initial read to see if receiver knows volume/input */
  amp_trigger_read();

  while (1)
  {
    watchdog_trigger();
    
    event = sm_event_get(&sm);

    printf ("Current state: %s (v=%d t=%d i=%s) event %s (v=%d i=%s)  ",
      state_names[sm.state], 
      sm.volume,
      sm.target,
      sm.input,
      event_names[event->event], 
      event->volume, 
      event->input);
    fflush(stdout);

    handler = sm_array[sm.state][event->event];

    if (!handler)
    {
      handler = sm_array[st_max_state][event->event];
    }

    if (handler)
    {
      enum sm_state prev_state = sm.state;

      sm.state = handler(&sm, event);

      while (prev_state != sm.state)
      {
        if (sm_array[prev_state][e_exit_state])
        {
          (void)sm_array[prev_state][e_exit_state](&sm, NULL);
        }

	mqtt_publish_string("state", (char*)state_names[sm.state], false);
        prev_state = sm.state;

        if (sm_array[sm.state][e_enter_state])
        {
          sm.state = sm_array[sm.state][e_enter_state](&sm, NULL);
        }
      }
    }

    printf ("-> %s\n", state_names[sm.state]);
  }
}
